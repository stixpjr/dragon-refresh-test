## Dragon/CoCo 32/64 data retention test

A program to help determine how long the DRAM can retain data without refresh.

It works by writing a pattern to memory then switching to fast mpu rate for a set time. After the set time has elapsed, the mpu rate is set back to normal and the pattern in memory is tested for changes. The number of failed memory locations is counted and displayed.

The program works with both 32K and 64K machines. The memory size is auto-detected but can be manually changed if required. The test time can be adjusted to find the point where the memory starts to accumulate errors.

The value of the last failed location is also displayed. This makes it possible to track down the weakest memory cell to a single memory IC, by adjusting the test time until just one fail is obtained.

Unfortunately some machines don't like running at double speed, so there's a chance this program won't work for everyone. I've tested three of my machines without problem.

### Included files

- `REFTEST.BIN`  DragonDos binary
- `reftest.wav`  wav audio file that can be loaded via a cassette lead
- `reftest.rom`  cartridge rom image
- `reftest.s`    source code

### Setup screen keys

- `1234/QWER` Adjust time digits up/down
- `M` toggle memory size between $8000 and $FF00 bytes
- `Z` zero the max fails counter
- `S` start one test cycle
- `C` perform continuous test cycles
- `BREAK` quit to BASIC

Pressing any key during a test will return to the setup screen

### More detail

The program protects itself from corruption by only being resident in memory rows that are refreshed by carefully controlled DRAM accesses during the delay part of the test. The program moves itself around in memory between tests to ensure that all memory is tested.

The are four different test patterns and two possible program locations meaning that the test should be run at least eight times to get full coverage.

### But why?

I wanted to find out if the video display accesses memory frequently enough to provide adequate memory refresh on its own. If so it would allow 256 row refresh DRAMs to be used without having to install the 74LS785 version of the SAM chip.

The specification for 256 row refresh is 4ms. The Dragon 32/64 text display cycles through all memory rows in about 14ms worst case, so at first glance that would appear to be not good enough. However, this program reveals that 41256 DRAMs can retain their contents for 3 seconds when warmed up, even longer when cold.

The 4116 type DRAMs in the 32K machine I tested lose data much faster, with errors starting to appear before about 350ms.

### How is there a text display in fast mode?

Good question!

Just because the SAM doesn't supply video data in fast mode doesn't mean we can't have a video display. You just need to ensure the mpu accesses the right data at the right times to generate a stable display :)

### Assembling from source

#### Prerequisites

- [asm6809 v2.12 or later](https://www.6809.org.uk/asm6809/) - 6809 assembler
- [bin2cas](https://www.6809.org.uk/dragon/#cas2wav) - for converting Dragon binary image to .cas or .wav cassette format
- [dzip](https://www.6809.org.uk/dragon/#dzip) - compression utility to speed up cassette loading time

Windows versions of bin2cas & dzip can be found [here](https://archive.worldofdragon.org/phpBB3/download/file.php?id=1680)


##### Build a DragonDos binary

```
$ asm6809 -D -o REFTEST.BIN reftest.s
```

##### Build a wav audio file for loading via the Dragon cassette port

```
$ asm6809 -D -o reftest.bin reftest.s
$ bin2cas.pl --autorun -z --fast -r 44100 -o reftest.wav -D reftest.bin
```

##### Build a rom image for an autorun cartridge

```
$ asm6809 -d PADROM -o reftest.rom reftest.s
```

##### Build a Tandy Color Computer (CoCo) binary

```
$ asm6809 -C -d COCO reftest.bin reftest.s
```

### Source code notes

The program is kind of spread out in memory as the memory rows it occupies are carefully controlled. This makes for an oversized binary image that is more than 75% empty space.

To counter this the source code contains directives and macros to pack the code blocks together, plus sets up an unpack routine that runs on program entry.

This probably doesn't matter so much with a dzip compressed cassette file, but it does make a big difference to the size of the .bin file.

### Acknowledgements

Ciaran Anscomb's [asm6809](https://www.6809.org.uk/asm6809/) is an exceptionally good assembler
